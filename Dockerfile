FROM        python:3.6

ARG         PROJECT_PATH=.

ADD         . ${PROJECT_PATH}

# python3 "$PWD/get_requirements.py"
RUN         pip3 install --upgrade -r ${PROJECT_PATH}/requirements.txt
            # && apk add --no-cache ca-certificates
            # && echo "Cleaning ..." \
            # && find ${PROJECT_PATH} -type f \( -iname ".gitmodules" -or -iname "*.md" -or -iname ".git" \) -delete \
            # && for d in $( find ${PROJECT_PATH} -type d \( -iname ".git" -or -iname ".vscode" -or -iname ".idea" -or -iname "docker" \) ) ; do rm -rf "$d"; \
            # done

WORKDIR     ${PROJECT_PATH}
VOLUME      /data/

CMD ["python3", "__main__.py"]
# ENTRYPOINT  ["python3", "-B", "__main__.py"]
