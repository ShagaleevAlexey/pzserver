from sanic import Sanic
from sanic.response import json as ResponseJson
from sanic.response import file as ResponseFile
from sanic.request import Request
import random
import string

app = Sanic()

@app.route('/archive', methods=['POST'])
def image_post(request):
    body = request.body
    headers = request.headers
    filename = ''.join(random.choices(string.ascii_uppercase + string.digits, k=24))
    filepassword = headers['filepassword']

    f = open(f'data/{filename}.zip', 'wb')

    f.write(body)

    f.close()

    p = open(f'data/{filename}.pass', 'w')

    p.writelines([filepassword])

    p.close()

    return ResponseJson({})

app.run(host='0.0.0.0', port=8000)